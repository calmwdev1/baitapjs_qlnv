var dsnv = [];

var dataJson = localStorage.getItem("DSNV");
if(dataJson != null){
    dsnv = JSON.parse(dataJson).map(function(item){
        return new NhanVien(
            item.tk,
            item.ten,
            item.email,
            item.matkhau,
            item.ngaylam,
            item.luong,
            item.chucvu,
            item.giolam
        );


    });
    renderDSNV(dsnv);
}

function themNhanVien(){

    var nv=laythongtintuform()
    //validate  TK
    var isvalid= kiemtratrung(nv.tk,dsnv)&& kiemtradodai(4,6,"tbTKNV","Tài Khoản phải từ 4 đến 6 ký tự",nv.tk);
    //validate tên nv
    isvalid= isvalid & tennhanvien(nv.ten);
    //validate email
    isvalid = isvalid & kiemtraemail(nv.email);
    //validate pw
    isvalid = isvalid & (kiemtradodai(6,10,"tbMatKhau","Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)",nv.matkhau) && kiemtramatkhau(nv.matkhau));
    //validate date
    isvalid = isvalid & kiemtrangay(nv.ngaylam);
    //validate luong
    isvalid = isvalid & kiemtraluong(nv.luong);
    //validate chucvu
    if(nv.chucvu=="Chọn chức vụ"){
        isvalid == false;
        showmessage("tbChucVu","Vui lòng chọn chức vụ")
    }
    else{
        showmessage("tbChucVu","");
    }
    //validate gio lam
    isvalid = isvalid & kiemtragio(nv.giolam);



    //de trong
    detrong(nv.tk,isvalid,"tbTKNV");
    detrong(nv.ten,isvalid,"tbTen");
    detrong(nv.email,isvalid,"tbEmail");
    detrong(nv.matkhau,isvalid,"tbMatKhau");
    detrong(nv.luong,isvalid,"tbLuongCB");
    detrong(nv.ngaylam,isvalid,"tbNgay");
    detrong(nv.giolam,isvalid,"tbGiolam");
    //xet bien
    if(isvalid){

    
    dsnv.push(nv);
    renderDSNV(dsnv);
// save dsnv localstorage
// json
    var dataJson= JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
    }
}
function xoaNhanVien(id){
    console.log(id);
    var index = dsnv.findIndex(function(item){
        console.log("item",item);
        return item.tk==id;
    })
    dsnv.splice(index,1);
    renderDSNV(dsnv);
}
function suaNhanVien(id){
    console.log(id);
    var index = dsnv.findIndex(function(item){
        console.log("item",item);
        return item.tk==id;

    });
   showinforlenform(dsnv[index]);
    //show thong tin len form

}


