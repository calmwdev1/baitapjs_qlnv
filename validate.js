function showmessage(idspan,message){
    document.getElementById(idspan).innerText= message;
}





function kiemtratrung(idnv,dsnv){
    var index = dsnv.findIndex(function(item){
        return item.tk == idnv;
    });
    if (index ==-1){
        showmessage("tbTKNV","");
        return true;
    }
    else{
        showmessage("tbTKNV", "TK nhân viên đã tồn tại");
        return false;
    }
}

function kiemtradodai(min, max, idspan, message, value){
    var length = value.length;
    if (length >= min && length <= max){
        showmessage(idspan,"")
        return true;
    }
    else{
        showmessage(idspan,message);
        return false;
    }
}
function detrong(value,item,idspan){
    if(value==""){
        item== false;
        document.getElementById(idspan).innerText="Không được để trống";

    }
}


// function removeAscent (str) {
//   if (str === null || str === undefined) return str;
//   str = str.toLowerCase();
//   str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
//   str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
//   str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
//   str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
//   str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
//   str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
//   str = str.replace(/đ/g, "d");
//   return str;
// }
function tennhanvien(value){
    const re=/[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u


    if(re.test(value)){
        showmessage("tbTen","");
        return true;
        
    }
    else{
        showmessage("tbTen","Tên phải là Chữ  ");
        return false;
    }
    

}
function kiemtraemail(email){
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if(re.test(email)){
    showmessage("tbEmail","");
    return true;
  }
  else{
    showmessage("tbEmail","Email phải đúng định dạng");
    return true;
  }
}
function kiemtramatkhau(pw){
    const re = /((?=.*\d)(?=.*[A-Z])(?=.*\W).{6,10})/;
    if(re.test(pw)){
    showmessage("tbMatKhau","");
    return true;
  }
  else{
    showmessage("tbMatKhau","Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)");
    return true;
  }
}
function kiemtrangay(date){
    const re = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
    if(re.test(date)){
    showmessage("tbNgay","");
    return true;
  }
  else{
    showmessage("tbNgay","Ngày phải đúng định dạng mm/dd/yyyy");
    return true;
  }

}
function kiemtraluong(luong){
    if (luong >= 1000000 && luong <= 20000000 ){
        showmessage("tbLuongCB","");
    return true;
    }
    else{
    showmessage("tbLuongCB","Lương cơ bản 1 000 000 - 20 000 000");
    return true;
  }
}
function kiemtragio(gio){
    if (gio >= 80 && gio <= 200 ){
        showmessage("tbGiolam","");
    return true;
    }
    else{
    showmessage("tbGiolam","Giờ làm phải từ 80-200");
    return true;
  }
}