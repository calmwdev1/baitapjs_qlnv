function renderDSNV(dsnv){
    var contentHTML="";
    for (var i=0 ; i<dsnv.length; i ++){
        var nv = dsnv[i];
        var content = ` <tr>
        <td> ${nv.tk}</td>
        <td> ${nv.ten}</td>
        <td> ${nv.email}</td>
        <td>${nv.ngaylam}</td>
        <td>${nv.chucvu}</td>
        <td>${nv.tongluong()}</td>
        <td>${nv.xeploai()}</td>
        <td> 
        <button onclick="xoaNhanVien('${nv.tk}')" class="btn btn-danger">Xóa</button>
        <button onclick="suaNhanVien('${nv.tk}')" class="btn btn-warning">Sửa</button>
        </td>
        </tr>`;
        contentHTML = contentHTML + content;
    }
    document.getElementById("tableDanhSach").innerHTML= contentHTML;
}
function showinforlenform(nv){
     document.getElementById("tknv").value=nv.tk
     document.getElementById("name").value=nv.ten
     document.getElementById("email").value=nv.email
     document.getElementById("password").value=nv.matkhau
     document.getElementById("datepicker").value=nv.ngaylam
     document.getElementById("luongCB").value =nv.luong;
     document.getElementById("chucvu").value=nv.chucvu;
     document.getElementById("gioLam").value=nv.giolam;
}
function laythongtintuform(){
    // lay thong tin tu form
    var tk = document.getElementById("tknv").value;
    var ten = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matkhau = document.getElementById("password").value;
    var ngaylam = document.getElementById("datepicker").value;
    var luong = document.getElementById("luongCB").value*1;
    var chucvu = document.getElementById("chucvu").value;
    var giolam = document.getElementById("gioLam").value*1;
    
    return new NhanVien(tk,ten,email,matkhau,ngaylam,luong,chucvu,giolam);
}