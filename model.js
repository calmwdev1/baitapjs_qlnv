function NhanVien(tk,ten,email,matkhau,ngaylam,luong,chucvu,giolam){
    this.tk = tk,
    this.ten= ten,
    this.email= email,
    this.matkhau= matkhau,
    this.ngaylam= ngaylam,
    this.luong= luong,
    this.chucvu= chucvu, 
    this.giolam= giolam, 
    
    this.tongluong = function(){
        if(this.chucvu=="Sếp"){
            return this.luong*3;
        }
        else if (this.chucvu=="Trưởng phòng"){
            return this.luong*2;
        }
        else{
            return this.luong;
        }
    }
    this.xeploai = function(){
        if (this.giolam >= 192)
        {
            return "Xuất Sắc";
        }
        else if(this.giolam >=176){
            return "Giỏi";
        }
        else if(this.giolam >=160){
            return "Khá";
        }
        else {
            return "Trung Bình";
        }
    }
}